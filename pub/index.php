<?php

require __DIR__ . '/../src/init.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('name');
$log->pushHandler(new StreamHandler(__DIR__ . '/../logs/app.log', Logger::DEBUG));
//
// add records to the log
$log->warning('For those about to log...');

echo "Hello world.\n";


