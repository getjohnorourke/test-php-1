<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'git@bitbucket.org:getjohnorourke/test-php-1.git');

// [Optional] Allocate tty for git clone. Default value is false.
//set('git_tty', true);

// Shared files/dirs between deploys
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server
set('writable_dirs', []);


// Hosts

host('159.65.194.95')
        ->user('t1')
        ->stage('production')
	->set('deploy_path', '~')
	->set('release_path', '{{deploy_path}}/htdocs');

// Tasks

desc('rsync the vendor folder');
task('deploy:copyvendors', function() {
	upload(__DIR__ . '/vendor/', '{{release_path}}/vendor');
});

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    //    'deploy:vendors', we don't need composer on the remote
    'deploy:copyvendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

task('ssh:showkeys', function() {
	run('ssh-add -l');
});

